# Neutron Box Image Viewer

## What is this?
My first Package! 

Install at your own risk as this is my first NPM package and I am playing with it. As it becomes an actual build I will update the notice and it would be ok to use. I am slowly adding more documentation and features as time goes on.

Eventually this will be an image viewer with lightbox-like functionality

## What it does

This module allows you to easily add larger image viewing to any Javascript application. Just add the images into your app and format them as needed, and the module will handle the rest.

## What it does NOT do yet

* Allow for viewing next/previous images
* Dynamic gallery imports

## How To Use

```
var NeutronBox = require('neutron-box')
```
or
```
import NeutronBox from 'neutron-box'
```
Once the package is loaded you can use the module by first adding images to your page, and then calling NeutronBox. Here is a simple example:
```
<img class="neutron-image" src="/img_thumb.jpg" data-large="/img.jpg" />
<img class="neutron-image" src="/img2_thumb.jpg" data-large="/img2.jpg"/>
        
NeutronBox({
  images: document.querySelectorAll('.neutron-image')
});
```
You can also utilize multiple class selection of 'querySelectorAll' to have the module read more than one group of images:
```
NeutronBox({
    images: document.querySelectorAll('.neutron-image, .neutron-image2')
});
```
THIS IS CURRENTLY IN THE WORKS AND DOES NOT DO ANYTHING YET... but in order to have images show in a gallery instead of just a single image, you must add the 'data-imagetype' attribute with a value of 'gallery':
```
<img class="neutron-image" src="/img3_thumb.jpg" data-imagetype="gallery" data-large="/img3.jpg" />
```
You can also implement this into React using componentDidMount. This will make sure the DOM content is there before looking for the image references:
```
componentDidMount() {
    NeutronBox({
      images: document.querySelectorAll('.neutron-image'),
      debug: true
    });
}

render() {
    return (
      <div>
        <img className="neutron-image" src={img1Small} data-large={img1} title="Sample" alt="Sample" />
        <img className="neutron-image" src={img2Small} data-large={img2} title="Sample2" alt="Sample2" />
      </div>
    );
}
```

## Configuration
Here is a list of the configuration parameters that can be passed into 'NeutronBox' as an object:


##### images
*Required*

Using document.querySelectAll, this parameter allows the application to see all of the images you want to apply the Neutron Box.

------

##### debug
Deafult: *false*

Tells the application if you want to run in debug mode. This will display data in the console for you.

------

##### imageScreenId 

Default: *'neutron-image-screen'*

The shown image screen. This is the div that covers the page while displaying the images inside.

------

##### imageBoxId

Default: *'neutron-image-box'*

The box holding the images. This is the div that will hold the images within the image screen.

------

##### imageId

Default: *'neutron-image'*

The ID of the single image being viewed. This is dynamically created by the application.

------

##### imageContainerId

Default: *'neutron-image-container'*

The ID of the image container where the single or gallery images will reside.

------

##### textId

Default: *'neutron-text'*

The ID of the single image text box (image title)

------

##### textPadding

Default: *50*

Additional padding (in pixels) for the vertical spacing. This allows for added space on the top and bottom in order to display the image and text appropriately. If you are not showing title text you can set this to 0. If the image is vertically 
going off the screen due to long image text, you can can increase this as needed.

------

##### singleImageAnimate

Default: *false*

By default the image/gallery will just show on the screen. Setting to true will give it a growing animation.

------

##### imageBoxShowSpeed

Default: *10*

The speed at which the images get shown in width increments. 'singleImageAnimate' setting must be set to true.

------

##### imageBoxPadding

Default: *35*

The padding allotted for the maximum size of the image to show. Once the image reaches within this pixel value from an edge, it will stop the rendering animation of the image.

------