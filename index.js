
import NeutronBox from './lib/NeutronBox';
import NeutronConfig from './lib/NeutronConfig';

var BuildBox = function BuildBox(options) {
  return new NeutronBox({ config: NeutronConfig(options) });
};

export default BuildBox;
