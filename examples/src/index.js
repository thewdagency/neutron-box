import NeutronBox from '../../src/index';

// Build our own require context to import all images
function importAll(r) {
  r.keys().forEach(r);
}
importAll(require.context('../assets/img/', true, /\.(png|jpg|jpeg|gif|svg)$/));

// Start NeutronBox
NeutronBox({
  images: document.querySelectorAll('.neutron-image, .neutron-image2'),
  debug: true
});
