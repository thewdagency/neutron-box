import SingleImage from './SingleImage';
import * as BoxActions from './boxActions';

/**
 * NeutronBox
 *
 * @param options
 * @constructor
 */
export default class NeutronBox {
  constructor(options) {
    this.config = options.config;
    this.config.imagesObj = { single: [], gallery: [] };

    this.neutronElements = {
      imgScreen: document.createElement('div'),
      imageText: document.createElement('div'),
      closeIcon: document.createElement('span'),
      imgBox: document.createElement('div'),
      imgContainer: document.createElement('div')
    };

    // Build an image object array for all images in the object request.
    // Once we have the images, we can start processing their data
    this.buildImagesObject()
      .then(() => {
        new SingleImage(this.config, this.neutronElements);
      })
      .catch((err) => {
        alert(err.message);
      });

    this.buildBoxes();
  }

  /**
   * Builds the imagesObj and assigns image data to each 'data-imagetype'
   *
   * @returns {Promise<any>}
   */
  buildImagesObject = () => (
    new Promise((resolve, reject) => {
      const numOfImgs = this.config.images.length;
      let imgCounter = 0;

      if (typeof this.config.images !== 'object') {
        reject(new Error('Images provided must be in a querySelectorAll object. (ex: document.querySelectorAll(\'.img-class-name\'))'));
      }

      this.config.images.forEach((el) => {
        // Build temp obj with the image data
        const tempImageObj = {
          domElement: el,
          urlThumb: el.getAttribute('src'),
          urlLarge: el.getAttribute('data-large'),
          title: el.getAttribute('title'),
          alt: el.getAttribute('alt')
        };

        // Depending on which data type, add it to the imagesObj
        if (el.getAttribute('data-imagetype') && el.getAttribute('data-imagetype') === 'gallery') {
          this.config.imagesObj.gallery.push(tempImageObj);
        } else {
          this.config.imagesObj.single.push(tempImageObj);
        }

        imgCounter += 1;

        if (numOfImgs === imgCounter) resolve();
      });
    })
  );

  /**
   * Builds the box areas for the images to reside
   */
  buildBoxes = () => {
    const self = this;

    // Set values to image screen
    this.neutronElements.imgScreen.setAttribute('id', this.config.imageScreenId);
    BoxActions.resetImageScreenStyles(this.neutronElements.imgScreen);

    // Append Image Screen to the body
    document.getElementsByTagName('body')[0].appendChild(this.neutronElements.imgScreen);

    // Add image text box
    BoxActions.resetTextBoxStyles(this.neutronElements.imageText, this.config.textId);
    this.neutronElements.imgBox.appendChild(this.neutronElements.imageText);

    // Set values to image box
    this.neutronElements.imgBox.setAttribute('id', this.config.imageBoxId);
    BoxActions.resetImageBoxStyles(this.neutronElements.imgBox);

    // Add the image box to the image screen
    this.neutronElements.imgScreen.appendChild(this.neutronElements.imgBox);

    // Add close icon to image box
    BoxActions.resetCloseIcon(this.neutronElements.closeIcon);
    this.neutronElements.imgBox.appendChild(this.neutronElements.closeIcon);

    // Assign click event to close icon and handle the visuals
    this.neutronElements.closeIcon.addEventListener('click', () => {
      BoxActions.resetImageBoxStyles(self.neutronElements.imgBox);
      BoxActions.resetImageScreenStyles(self.neutronElements.imgScreen);
      BoxActions.emptyImageContainer(self.config.imageContainerId);
    });

    // Create the image container
    this.neutronElements.imgContainer.setAttribute('id', this.config.imageContainerId);
    this.neutronElements.imgBox.appendChild(this.neutronElements.imgContainer);
  };
}
