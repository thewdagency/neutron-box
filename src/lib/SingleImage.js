import ShowSingleImage from './ShowSingleImage';
import * as BoxActions from './boxActions';

/**
 * SingleImage
 */

export default class SingleImage {
  constructor(config, neutronElements) {
    this.config = config;
    this.neutronElements = neutronElements;

    this.assignOnclicks();
  }

  /**
   * Assign onclicks to all single images
   */
  assignOnclicks = () => {
    const self = this;

    this.config.imagesObj.single.forEach((img) => {
      img.domElement.addEventListener('click', () => {
        self.openLarge(img.domElement.getAttribute('data-large'), img.domElement.getAttribute('title'));
      });
    });
  };

  /**
   * Onclick function for the images
   *
   * @param lrgImageUrl
   * @param imageTitle
   */
  openLarge = (lrgImageUrl, imageTitle) => {
    // Remove the image if there is one
    BoxActions.emptyImageContainer();

    // Setup interval for growing the image.
    // Wait until the image dimensions are retrieved before processing resize
    this.config.getImageDimensions(lrgImageUrl)
      .then((theDimensions) => {
        // Build the image
        const singleImage = document.createElement('img');
        singleImage.setAttribute('src', lrgImageUrl);
        singleImage.setAttribute('id', this.config.imageId);
        singleImage.style.width = '100%';
        this.neutronElements.imgContainer.appendChild(singleImage);

        // Set title text to the text box. If no title, "hide" it
        if (imageTitle) {
          BoxActions.resetTextBoxStyles(this.neutronElements.imageText, this.config.textId);
          this.neutronElements.imageText.innerHTML = imageTitle;
        } else {
          this.neutronElements.imageText.innerHTML = '';
          this.neutronElements.imageText.style.padding = '0';
        }

        // Update settings for image screen
        BoxActions.showImageScreen(this.neutronElements.imgScreen);

        // Show the image box
        this.neutronElements.imgBox.style.display = 'block';

        const showImageData = {
          config: this.config,
          singleImage,
          imgBox: this.neutronElements.imgBox,
          theDimensions
        };

        new ShowSingleImage(showImageData);
      })
      .catch((error) => {
        alert(error.message);
      });
  };
}
