/**
 * NeutronConfig.js
 *
 * Sets up and defines the configurations for the Neutron Box
 *
 * @param options
 * @constructor
 */

const NeutronConfig = options => ({
  debug: options.debug || false,
  images: options.images,
  imageScreenId: options.imageScreenId || 'neutron-image-screen',
  imageBoxId: options.imageBoxId || 'neutron-image-box',
  imageId: options.imageId || 'neutron-image',
  imageContainerId: options.imageContainerId || 'neutron-image-container',
  textId: options.textId || 'neutron-text',
  textPadding: options.textPadding || 50,
  singleImageAnimate: options.singleImageAnimate || false,
  imageBoxShowSpeed: options.imageBoxShowSpeed || 10,
  imageBoxPadding: options.imageBoxPadding || 35,

  /**
   * Takes an image URL and returns the dimensions. It will return a promise
   * containing the image dimensions after the image is loaded.
   *
   * @param imgUrl
   */
  getImageDimensions: imgUrl => (
    new Promise((resolve, reject) => {
      const newImg = new Image();
      const imageDimensions = {};

      // Wait until image is loaded in order to get the dimensions
      newImg.onload = () => {
        imageDimensions.height = newImg.height;
        imageDimensions.width = newImg.width;

        resolve(imageDimensions);
      };

      // Image doesn't load, return rejection notice
      newImg.onerror = () => {
        reject(new Error('Image failed to load'));
      };

      newImg.src = imgUrl;
    })
  )
});

export default NeutronConfig;
