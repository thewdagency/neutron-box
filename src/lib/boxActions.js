/**
 * Displays the image screen to cover site content
 */
export const showImageScreen = (imageScreenDiv) => {
  const tempDiv = imageScreenDiv;

  tempDiv.style.top = '0';
  tempDiv.style.left = '0';
  tempDiv.style.right = '0';
  tempDiv.style.bottom = '0';

  // Show the image screen
  tempDiv.style.display = 'block';
};

/**
 * Empty out the contents of the image container
 *
 * @param imageContainerId
 */
export const emptyImageContainer = (imageContainerId) => {
  const containerDiv = document.getElementById(imageContainerId);
  if (containerDiv) containerDiv.innerHTML = '';
};

/**
 * Resets the imageScreen styles
 */
export const resetImageScreenStyles = (s) => {
  const imgScreen = s;

  imgScreen.style.display = 'none';
  imgScreen.style.backgroundColor = 'rgba(0,0,0,0.6)';
  imgScreen.style.position = 'absolute';
  imgScreen.style.zIndex = '998';
};

/**
 * Sets styling for close icon
 */
export const resetCloseIcon = (c) => {
  const closeIcon = c;

  closeIcon.innerText = 'X';
  closeIcon.style.cursor = 'pointer';
  closeIcon.style.position = 'absolute';
  closeIcon.style.top = '-16px';
  closeIcon.style.right = '-13px';
  closeIcon.style.backgroundColor = '#fff';
  closeIcon.style.borderRadius = '20px';
  closeIcon.style.padding = '10px 14px';
  closeIcon.style.boxShadow = '1px 2px 13px #666';
};

/**
 * Resets the imageText styles
 */
export const resetTextBoxStyles = (t, id) => {
  const imageText = t;

  imageText.setAttribute('id', id);
  imageText.style.backgroundColor = '#fff';
  imageText.style.padding = '10px 14px';
};

/**
 * Resets the imgBox styles
 */
export const resetImageBoxStyles = (i) => {
  const imgBox = i;

  imgBox.style.display = 'none';
  imgBox.style.width = '0';
  imgBox.style.height = '0';
  imgBox.style.position = 'relative';
  imgBox.style.zIndex = '999';
  imgBox.style.top = '50%';
  imgBox.style.left = '50%';
  imgBox.style.marginTop = '0'; // When animating, needs to be half the height
  imgBox.style.marginLeft = '0'; // When animating, needs to be half the width
};
