/**
 * ShowSingleImage.js
 *
 * This function will take arguments in order to enlarge the image to the
 * required specifications.
 */
export default class ShowSingleImage {
  constructor(values) {
    const SSI = this;
    this.config = values.config;
    this.theDimensions = values.theDimensions;
    this.singleImage = values.singleImage;
    this.imgBoxDiv = values.imgBox;
    this.currentWidth = 0;
    this.maxWidth = window.innerWidth - (this.config.imageBoxPadding * 2);
    this.maxHeight = window.innerHeight - ((this.config.imageBoxPadding * 2) + this.config.textPadding);

    if (this.config.singleImageAnimate) {
      // Start interval for growing
      this.showInterval = setInterval(() => { SSI.growImageBox(); }, 5);
    } else {
      // Just show image
      this.showImageBox();
    }
  }

  /**
   * Show image box WITHOUT grow resizing
   */
  showImageBox = () => {
    // console.log('Dimensions width: ', this.theDimensions.width);
    // console.log('Dimensions height: ', this.theDimensions.height);
    // Find out which is larger (width or height) to see how we should decide final image size
    const measureBy = this.theDimensions.width >= this.theDimensions.height ? 'width' : 'height';
    let widthToUse = 0;

    if (measureBy === 'width') {
      widthToUse = this.theDimensions.width <= this.maxWidth ? this.theDimensions.width : this.maxWidth;
    } else if (measureBy === 'height') {
      const heightToUse = this.theDimensions.height <= this.maxHeight ? this.theDimensions.height : this.maxHeight;
      const ratio = heightToUse / this.theDimensions.height; // Get ratio of scaled image
      const imgNewWidth = this.theDimensions.width * ratio; // New image with at the ratio
      widthToUse = imgNewWidth <= this.maxWidth ? imgNewWidth : this.maxWidth; // Check which one will fit on the screen
    }

    // Set all values for the image
    this.imgBoxDiv.style.width = `${widthToUse}px`;
    this.imgBoxDiv.style.marginTop = `-${this.singleImage.height / 2}px`;
    this.imgBoxDiv.style.marginLeft = `-${widthToUse / 2}px`;
  };

  /**
   * The interval function for showing an image grow
   */
  growImageBox = () => {
    if (this.currentWidth >= this.maxWidth
        || this.singleImage.height >= this.maxHeight
        || this.currentWidth >= this.theDimensions.width
        || this.singleImage.height >= this.theDimensions.height) {
      if (this.config.debug) {
        console.log({
          'Current Width': this.currentWidth,
          'Current Height': this.singleImage.height,
          'Max Width': this.maxWidth,
          'Max Height': this.maxHeight
        });
      }

      clearInterval(this.showInterval);
    } else {
      this.currentWidth += this.config.imageBoxShowSpeed;
      this.imgBoxDiv.style.width = `${this.currentWidth}px`;
      this.imgBoxDiv.style.marginTop = `-${this.singleImage.height / 2}px`;
      this.imgBoxDiv.style.marginLeft = `-${this.currentWidth / 2}px`;
    }
  };
}