/**
 * index.js
 *
 * Index file for the Neutron Box Module
 */
import NeutronBox from './lib/NeutronBox';
import NeutronConfig from './lib/NeutronConfig';

const BuildBox = options => (
  new NeutronBox({ config: NeutronConfig(options) })
);

export default BuildBox;
